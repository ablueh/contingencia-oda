#! /usr/bin/ksh
#=========================================================================================================================
# MTA - Grupo Telecom Americas
#
# Nome    : CONTINGENCIA_ODA_0001.sh
# Objetivo: 
#
# Historico:
#
#
#=========================================================================================================================
#----------------------------------------------------------
#  Inicializa variaveis de Trabalho
#----------------------------------------------------------
DTLOG=$(date +%Y%m%d%H%M)

YEAR=$(echo ${DTLOG} | cut -c1-4)
MONTH=$(echo ${DTLOG} | cut -c5-6)
DAY=$(echo ${DTLOG} | cut -c7-8)

# --------------------------------------
# Aplicativos Externos
# --------------------------------------
FILE_DIR_JAVA=/usr/bin

# --------------------------------------
# Diretorios e Arquivos do Projeto
# --------------------------------------
FILE_DIR_BASE=/home/alexandre/proj/claro/prepago/contingencia-oda/target/contingencia-oda-0.1
FILE_DIR_LOG=${FILE_DIR_BASE}/log/${YEAR}/${MONTH}/${DAY}
FILE_DIR_BIN=${FILE_DIR_BASE}/bin
FILE_DIR_LIB=${FILE_DIR_BASE}/lib
FILE_DIR_CFG=${FILE_DIR_BASE}/cfg
LOG=${FILE_DIR_LOG}/CONTINGENCIA_ODA_0001_${DTLOG}.log

#----------------------------------------------------------
#  Configura CLASSPATH
#----------------------------------------------------------
CLASSPATH=${FILE_DIR_CFG}
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/claro-service-locator-3.0.0.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/commons-dbcp-1.4.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/commons-lang-2.6.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/commons-logging-1.2.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/commons-pool-1.5.4.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/ejb-api-3.0.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/log4j-1.2.17.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/ojdbc14-10.2.0.4.0.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/ServicesPlatformsClient-1.0.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/slf4j-api-1.7.12.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/slf4j-log4j12-1.7.12.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_LIB}/wlclient-10.3.5.jar
CLASSPATH=${CLASSPATH}:${FILE_DIR_BIN}/contingencia-oda-0.1.jar
MAIN_CLASS=br.com.claro.contingenciaoda.Runner

#----------------------------------------------------------
#  Configura ARGS
#----------------------------------------------------------
ARGS=$*;

mkdir -p ${FILE_DIR_LOG}

log() {

  echo "[$(date '+%d/%m/%Y %H:%M:%S')] $1" | tee -a ${LOG}

}

#----------------------------------------------------------
#  Inicia Rotina
#----------------------------------------------------------
log "INFO: Inicio da Execucao"

#----------------------------------------------------------
#  Verifica e Remove Rotinas em Execucao
#----------------------------------------------------------
CONT=0
ps -efx | grep ${LOG} | grep -v grep
while [ $? -eq 0 ]
do
   ps -efx | grep ${LOG} | grep -v grep | awk '{ print $2 }' | xargs -l kill -SIGTERM
   if [ $CONT -gt 3 ]
   then
       log "ERROR: Demora excessiva no stop do CONTINGENCIA_ODA_0001.sh"
       exit 1
   fi
         CONT=`expr $CONT + 1`
         sleep 10
         ps -efx | grep ${LOG} | grep -v grep
done

#----------------------------------------------------------
#  Executa Batch Java
#----------------------------------------------------------
log `${FILE_DIR_JAVA}/java -version`
${FILE_DIR_JAVA}/java -Duser.timezone=GMT-3 -Xms512m -Xmx1024m -Xmn64m -classpath ${CLASSPATH} ${MAIN_CLASS} ${ARGS}

if [ $? -eq 0 ]; then 
  log "INFO: Classe java executada com Sucesso"
else 
  log "ERROR: Falha na execucao da Classe java"
  exit 1
fi

#----------------------------------------------------------
#  Finaliza Rotina
#----------------------------------------------------------
log "INFO: Fim da execucao"

