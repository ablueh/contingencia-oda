package br.com.claro.contingenciaoda.domain;

import java.util.Date;

public class Item {

    // COD_SIMCARD VARCHAR2(30)
    private String codigoSimcard;

    // NRO_TELEFONE VARCHAR2(20)
    private String numeroTelefone;

    // COD_IMSI_TEMP VARCHAR2(30)
    private String codigoIMSITemporario;

    // COD_IMSI VARCHAR2(30)
    private String codigoIMSI;

    // IND_ESTADO VARCHAR2(30)
    private String estado;

    // DTA_CRIACAO DATE
    private Date dataCriacao;

    // DTA_ATUALIZACAO DATE Y
    private Date dataAtualizacao;

    // DES_ERRO VARCHAR2(3000) Y
    private String descricaoErro;

    public String getCodigoIMSI() {
        return codigoIMSI;
    }

    public String getCodigoIMSITemporario() {
        return codigoIMSITemporario;
    }

    public String getCodigoSimcard() {
        return codigoSimcard;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public String getDDD() {
        return null == getNumeroTelefone() ? null : getNumeroTelefone().substring(0, 2);
    }

    public String getDescricaoErro() {
        return descricaoErro;
    }

    public String getDescricaoErro(int limit) {
        return null == descricaoErro ? null
                : descricaoErro.length() <= limit ? descricaoErro : descricaoErro.substring(0, limit);
    }

    public String getEstado() {
        return estado;
    }

    public String getNumeroTelefone() {
        return numeroTelefone;
    }

    public void setCodigoIMSI(String codigoIMSI) {
        this.codigoIMSI = codigoIMSI;
    }

    public void setCodigoIMSITemporario(String codigoIMSITemporario) {
        this.codigoIMSITemporario = codigoIMSITemporario;
    }

    public void setCodigoSimcard(String codigoSimcard) {
        this.codigoSimcard = codigoSimcard;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public void setDescricaoErro(String descricaoErro) {
        this.descricaoErro = descricaoErro;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setNumeroTelefone(String numeroTelefone) {
        this.numeroTelefone = numeroTelefone;
    }
}
