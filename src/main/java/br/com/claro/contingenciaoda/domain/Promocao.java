package br.com.claro.contingenciaoda.domain;

public class Promocao {

    private String planoPreco;

    private String bitSOB;

    private String mascaraSOB;

    private Double valorVoucher;

    private String codigoVoucher;

    private String idPCRF;

    public Promocao(String planoPreco, String bitSOB, String mascaraSOB, Double valorVoucher, String codigoVoucher,
            String idPCRF) {
        this.planoPreco = planoPreco;
        this.bitSOB = bitSOB;
        this.mascaraSOB = mascaraSOB;
        this.valorVoucher = valorVoucher;
        this.codigoVoucher = codigoVoucher;
        this.idPCRF = idPCRF;
    }

    public String getBitSOB() {
        return bitSOB;
    }

    public String getCodigoVoucher() {
        return codigoVoucher;
    }

    public String getIdPCRF() {
        return idPCRF;
    }

    public String getMascaraSOB() {
        return mascaraSOB;
    }

    public String getPlanoPreco() {
        return planoPreco;
    }

    public Double getValorVoucher() {
        return valorVoucher;
    }

    public void setBitSOB(String bitSOB) {
        this.bitSOB = bitSOB;
    }

    public void setCodigoVoucher(String codigoVoucher) {
        this.codigoVoucher = codigoVoucher;
    }

    public void setIdPCRF(String idPCRF) {
        this.idPCRF = idPCRF;
    }

    public void setMascaraSOB(String mascaraSOB) {
        this.mascaraSOB = mascaraSOB;
    }

    public void setPlanoPreco(String planoPreco) {
        this.planoPreco = planoPreco;
    }

    public void setValorVoucher(Double valorVoucher) {
        this.valorVoucher = valorVoucher;
    }

    @Override
    public String toString() {
        return String.format("Plano Preço: %s, bit: %s, mascara: %s, voucher: %.2f, codigo: %s, PCRF: %s", planoPreco, bitSOB,
                mascaraSOB, valorVoucher, codigoVoucher, idPCRF);
    }

}
