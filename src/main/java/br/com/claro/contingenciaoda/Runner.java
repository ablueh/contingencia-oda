package br.com.claro.contingenciaoda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.claro.contingenciaoda.processor.BatchProcessor;

public class Runner {
    private static final Logger log = LoggerFactory.getLogger(Runner.class.getName());

    public static void main(String[] args) {
        log.info(">>> PROCESSO CONTINGÊNCIA ODA <<<");
        BatchProcessor.getDefault().execute();
    }

}
