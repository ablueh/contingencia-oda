package br.com.claro.contingenciaoda.service;

import br.com.claro.servicesplatforms.facade.BroadbandService;
import br.com.claro.servicesplatforms.facade.BroadbandServiceHome;
import br.com.claro.servicesplatforms.facade.ChargeSystemService;
import br.com.claro.servicesplatforms.facade.ChargeSystemServiceHome;
import br.com.claro.util.locator.ServiceLocator;
import br.com.claro.util.locator.ServiceLocatorFactory;

public class PrePaidService {

    static ServiceLocator serviceLocator = ServiceLocatorFactory.getServiceLocator("ServiceLocator");

    private static PrePaidService singleton;

    public static BroadbandService broadBand() {
        return getDefault().getBroadbandService();
    }

    public static ChargeSystemService chargeSystem() {
        return getDefault().getChargeSystemService();
    }

    private static PrePaidService getDefault() {
        if (null == singleton) singleton = new PrePaidService();
        return singleton;
    }

    // private ChargeSystemService chargeSystemService;

    // private BroadbandService broadbandService;

    private BroadbandService getBroadbandService() {
        // if (null == broadbandService) {
        try {
            BroadbandServiceHome home = (BroadbandServiceHome) serviceLocator.getRemoteHome(BroadbandServiceHome.JNDI_NAME,
                    BroadbandServiceHome.class);
            // broadbandService = home.create();
            return home.create();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        // }
        // return broadbandService;
    }

    private ChargeSystemService getChargeSystemService() {
        // if (null == chargeSystemService) {
        try {
            ChargeSystemServiceHome home = (ChargeSystemServiceHome) serviceLocator
                    .getRemoteHome(ChargeSystemServiceHome.JNDI_NAME, ChargeSystemServiceHome.class);
            // chargeSystemService = home.create();
            return home.create();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        // }
        // return chargeSystemService;
    }

}
