package br.com.claro.contingenciaoda.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class DAO {

    private static final Logger log = LoggerFactory.getLogger(DAO.class.getName());

    private BasicDataSource dataSource;

    protected void close(Object... closeables) {
        for (Object closeable : closeables) {
            if (null != closeable) try {
                if (closeable instanceof ResultSet)
                    ((ResultSet) closeable).close();
                else if (closeable instanceof PreparedStatement)
                    ((PreparedStatement) closeable).close();
                else if (closeable instanceof Connection) ((Connection) closeable).close();
            } catch (Throwable t) {}
        }
    }

    protected Connection getConnection() throws SQLException {
        if (null == dataSource) init();
        return dataSource.getConnection();
    }

    private void init() {
        Properties props = new Properties();
        try {
            props.load(getClass().getResourceAsStream("/database.properties"));
        } catch (IOException e) {
            throw new RuntimeException("Arquivo database.properties não foi encontrado no CLASSPATH");
        }

        String driver = props.getProperty("driver");
        String url = props.getProperty("url");
        String username = props.getProperty("username");
        String password = props.getProperty("password");
        Integer maxConnections = Integer.valueOf(props.getProperty("maxConnections", "10"));

        log.info(String.format("Conectando ao banco de dados: diver: %s, URL: %s, username: %s", driver, url, username));

        dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setPassword(password);
        dataSource.setUsername(username);
        dataSource.setUrl(url);
        dataSource.setMaxActive(maxConnections);
    }
}
