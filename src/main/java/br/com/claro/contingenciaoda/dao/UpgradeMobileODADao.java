package br.com.claro.contingenciaoda.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.claro.contingenciaoda.domain.Item;

public class UpgradeMobileODADao extends DAO {
    private static UpgradeMobileODADao singleton;

    public static UpgradeMobileODADao getDefault() {
        if (null == singleton) singleton = new UpgradeMobileODADao();
        return singleton;
    }

    public List<Item> getItemsToProcess() throws SQLException {
        List<Item> result = new ArrayList<Item>();
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            st = con.prepareStatement(
                    "select COD_SIMCARD, NRO_TELEFONE, COD_IMSI_TEMP, COD_IMSI, IND_ESTADO, DTA_CRIACAO, DTA_ATUALIZACAO, DES_ERRO from SPS2NACO.SPS_UPGRADE_MOBILE_ODA where IND_ESTADO = ?");
            st.setString(1, "PENDENTE_PROMOCAO");
            rs = st.executeQuery();
            while (rs.next()) {
                Item item = new Item();
                item.setCodigoSimcard(rs.getString("COD_SIMCARD"));
                item.setNumeroTelefone(rs.getString("NRO_TELEFONE"));
                item.setCodigoIMSITemporario(rs.getString("COD_IMSI_TEMP"));
                item.setCodigoIMSI(rs.getString("COD_IMSI"));
                item.setEstado(rs.getString("IND_ESTADO"));
                item.setDataCriacao(rs.getTimestamp("DTA_CRIACAO"));
                item.setDataAtualizacao(rs.getTime("DTA_ATUALIZACAO"));
                item.setDescricaoErro(rs.getString("DES_ERRO"));
                result.add(item);
            }
        } finally {
            close(rs, st, con);
        }
        return result;
    }

    public void saveItem(Item item) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            st = con.prepareStatement(
                    "update SPS2NACO.SPS_UPGRADE_MOBILE_ODA set IND_ESTADO = ?, DTA_ATUALIZACAO = SYSDATE, DES_ERRO = ? where NRO_TELEFONE = ?");
            st.setString(1, item.getEstado());
            st.setString(2, item.getDescricaoErro(2500));
            st.setString(3, item.getNumeroTelefone());
            st.executeUpdate();
        } finally {
            close(rs, st, con);
        }
    }

}
