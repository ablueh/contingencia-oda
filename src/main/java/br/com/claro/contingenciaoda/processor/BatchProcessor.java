package br.com.claro.contingenciaoda.processor;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.claro.contingenciaoda.dao.UpgradeMobileODADao;
import br.com.claro.contingenciaoda.domain.Item;

public class BatchProcessor {
    private static BatchProcessor singleton;

    private static final Logger log = LoggerFactory.getLogger(BatchProcessor.class.getName());

    public static BatchProcessor getDefault() {
        if (null == singleton) singleton = new BatchProcessor();
        return singleton;
    }

    private BatchProcessor() {
    }

    public void execute() {
        List<Item> items = getItems();

        if (items.isEmpty()) {
            log.info("Nada a processar");
            return;
        }

        log.info(String.format("Iniciando processamento de %d itens", items.size()));
        long start = System.currentTimeMillis();
        try {

            int threads = Integer.parseInt(System.getProperty("oda-threads", "15"));

            ExecutorService threadPool = Executors.newFixedThreadPool(threads);

            for (Item item : items) {
                threadPool.execute(new ItemProcessor(item));
            }

            threadPool.shutdown();

            log.info("Aguardando threads terminarem");

            try {
                threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
            } catch (InterruptedException e) {
                throw new RuntimeException("Ocorreu erro enquanto aguardava as threads terminarem", e);
            }
        } finally {
            log.info(String.format("Processamento terminou em %d ms.", System.currentTimeMillis() - start));
        }
    }

    private List<Item> getItems() {
        try {
            return UpgradeMobileODADao.getDefault().getItemsToProcess();
        } catch (SQLException e) {
            throw new RuntimeException("Ocorreu um erro ao buscar a lista de itens a serem processados", e);
        }
    }

}
