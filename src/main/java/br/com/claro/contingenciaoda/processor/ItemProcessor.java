package br.com.claro.contingenciaoda.processor;

import java.sql.SQLException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.claro.contingenciaoda.dao.UpgradeMobileODADao;
import br.com.claro.contingenciaoda.domain.Item;
import br.com.claro.contingenciaoda.domain.Promocao;
import br.com.claro.contingenciaoda.exceptions.PrePaidProvisionException;
import br.com.claro.contingenciaoda.service.PrePaidService;
import br.com.claro.servicesplatforms.remote.SubscriberType;

public class ItemProcessor implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(ItemProcessor.class.getName());

    private static final Promocao PROMOCAO1 = new Promocao("508", null, null, 0D, "I1", "201504221101");

    private static final Promocao PROMOCAO2 = new Promocao("506", "1069420", "2097151", 0D, "I1", "201504221101");

    private final Item item;

    public ItemProcessor(Item item) {
        this.item = item;
    }

    private void applyPromocao(Promocao promocao) {
        if (null == promocao) return;

        log.info(String.format("Aplicando promocao [%s] para o DDD [%s]", promocao.toString(), item.getDDD()));

        try {
            provision(item.getNumeroTelefone(), promocao);
            item.setEstado("FINALIZADO");
        } catch (PrePaidProvisionException e) {
            item.setEstado("FALHA_PROMOCAO");
            item.setDescricaoErro(ExceptionUtils.getFullStackTrace(e));
        }

        try {
            UpgradeMobileODADao.getDefault().saveItem(item);
        } catch (SQLException e) {
            log.error(String.format("Falha ao atualizar item: %s", item.getNumeroTelefone()), e);
        }
    }

    private void provision(String msisdn, Promocao promocao) throws PrePaidProvisionException {
        if (null == msisdn) throw new PrePaidProvisionException("MSISDN inválido", null);

        msisdn = "55".concat(msisdn);

        // Trocar plano preco
        try {
            PrePaidService.chargeSystem().updateAccountServiceClass(msisdn, SubscriberType.PREPAID, null,
                    promocao.getPlanoPreco());
        } catch (Exception e) {
            throw new PrePaidProvisionException("Falha ao aplicar Plano Preço", e);
        }

        if (null != promocao.getBitSOB()) try {
            // BIT
            PrePaidService.chargeSystem().updateSubscriberServiceOffering(msisdn, promocao.getBitSOB(),
                    promocao.getMascaraSOB());
        } catch (Exception e) {
            throw new PrePaidProvisionException("Falha ao aplicar BIT", e);
        }

        // voucher
        try {
            PrePaidService.chargeSystem().refill(msisdn, SubscriberType.PREPAID, promocao.getValorVoucher(), "BRL",
                    promocao.getCodigoVoucher(), "1", null, "1");
        } catch (Exception e) {
            // throw new PrePaidProvisionException("Falha ao aplicar Voucher", e);
            log.warn("Falha ao aplicar Voucher", e);
        }

        // Plano de dados
        try {
            PrePaidService.broadBand().addSubscriptionToSubscriber(msisdn, promocao.getIdPCRF(), false, "PEDCLI", "NGP", "NGP");
        } catch (Exception e) {
            throw new PrePaidProvisionException("Falha ao aplicar Plano de Dados", e);
        }

    }

    @Override
    public void run() {
        if (null == item) return;

        log.info(String.format("Processando telefone %s", item.getNumeroTelefone()));

        String ddd = item.getDDD();

        if (ddd.matches("^[1245678]\\d|3[123]$")) {
            applyPromocao(PROMOCAO1);
        } else if (ddd.matches("^9\\d|3[4578]$")) { // FIXME: verificar se o 36 realmente deve estar fora da regra
            applyPromocao(PROMOCAO2);
        }

        log.info(String.format("Telefone processado. Status = %s", item.getEstado()));
    }

}
