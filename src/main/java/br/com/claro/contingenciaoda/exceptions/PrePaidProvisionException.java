package br.com.claro.contingenciaoda.exceptions;

public class PrePaidProvisionException extends Exception {

    private static final long serialVersionUID = -4125205960310091284L;

    public PrePaidProvisionException(String message, Exception cause) {
        super(message, cause);
    }

}
