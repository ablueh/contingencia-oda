import org.junit.Assert;
import org.junit.Test;

public class RegexTest {

    private int getPromocao(String ddd) {
        if (ddd.matches("^[1245678]\\d|3[123]$")) {
            // if (ddd.matches("^[1245678]\\d|3[123]$")) {
            return 1;
        } else if (ddd.matches("^9\\d|3[4578]$")) {
            return 2;
        }
        return 0;
    }

    @Test
    public void test() {
        Assert.assertEquals(1, getPromocao("11"));
        Assert.assertEquals(1, getPromocao("12"));
        Assert.assertEquals(1, getPromocao("13"));
        Assert.assertEquals(1, getPromocao("14"));
        Assert.assertEquals(1, getPromocao("15"));
        Assert.assertEquals(1, getPromocao("16"));
        Assert.assertEquals(1, getPromocao("17"));
        Assert.assertEquals(1, getPromocao("18"));
        Assert.assertEquals(1, getPromocao("19"));
        Assert.assertEquals(1, getPromocao("20"));
        Assert.assertEquals(1, getPromocao("21"));
        Assert.assertEquals(1, getPromocao("22"));
        Assert.assertEquals(1, getPromocao("23"));
        Assert.assertEquals(1, getPromocao("24"));
        Assert.assertEquals(1, getPromocao("25"));
        Assert.assertEquals(1, getPromocao("26"));
        Assert.assertEquals(1, getPromocao("27"));
        Assert.assertEquals(1, getPromocao("28"));
        Assert.assertEquals(1, getPromocao("29"));

        Assert.assertEquals(1, getPromocao("31"));
        Assert.assertEquals(1, getPromocao("32"));
        Assert.assertEquals(1, getPromocao("33"));
        Assert.assertEquals(2, getPromocao("34"));
        Assert.assertEquals(2, getPromocao("35"));
        // Assert.assertEquals(1, getPromocao("36"));
        Assert.assertEquals(2, getPromocao("37"));
        Assert.assertEquals(2, getPromocao("38"));
        // Assert.assertEquals(1, getPromocao("39"));

        Assert.assertEquals(1, getPromocao("40"));
        Assert.assertEquals(1, getPromocao("41"));
        Assert.assertEquals(1, getPromocao("42"));
        Assert.assertEquals(1, getPromocao("43"));
        Assert.assertEquals(1, getPromocao("44"));
        Assert.assertEquals(1, getPromocao("45"));
        Assert.assertEquals(1, getPromocao("46"));
        Assert.assertEquals(1, getPromocao("47"));
        Assert.assertEquals(1, getPromocao("48"));
        Assert.assertEquals(1, getPromocao("49"));

        Assert.assertEquals(1, getPromocao("50"));
        Assert.assertEquals(1, getPromocao("51"));
        Assert.assertEquals(1, getPromocao("52"));
        Assert.assertEquals(1, getPromocao("53"));
        Assert.assertEquals(1, getPromocao("54"));
        Assert.assertEquals(1, getPromocao("55"));
        Assert.assertEquals(1, getPromocao("56"));
        Assert.assertEquals(1, getPromocao("57"));
        Assert.assertEquals(1, getPromocao("58"));
        Assert.assertEquals(1, getPromocao("59"));

        Assert.assertEquals(1, getPromocao("60"));
        Assert.assertEquals(1, getPromocao("61"));
        Assert.assertEquals(1, getPromocao("62"));
        Assert.assertEquals(1, getPromocao("63"));
        Assert.assertEquals(1, getPromocao("64"));
        Assert.assertEquals(1, getPromocao("65"));
        Assert.assertEquals(1, getPromocao("66"));
        Assert.assertEquals(1, getPromocao("67"));
        Assert.assertEquals(1, getPromocao("68"));
        Assert.assertEquals(1, getPromocao("69"));

        Assert.assertEquals(1, getPromocao("70"));
        Assert.assertEquals(1, getPromocao("71"));
        Assert.assertEquals(1, getPromocao("72"));
        Assert.assertEquals(1, getPromocao("73"));
        Assert.assertEquals(1, getPromocao("74"));
        Assert.assertEquals(1, getPromocao("75"));
        Assert.assertEquals(1, getPromocao("76"));
        Assert.assertEquals(1, getPromocao("77"));
        Assert.assertEquals(1, getPromocao("78"));
        Assert.assertEquals(1, getPromocao("79"));

        Assert.assertEquals(1, getPromocao("80"));
        Assert.assertEquals(1, getPromocao("81"));
        Assert.assertEquals(1, getPromocao("82"));
        Assert.assertEquals(1, getPromocao("83"));
        Assert.assertEquals(1, getPromocao("84"));
        Assert.assertEquals(1, getPromocao("85"));
        Assert.assertEquals(1, getPromocao("86"));
        Assert.assertEquals(1, getPromocao("87"));
        Assert.assertEquals(1, getPromocao("88"));
        Assert.assertEquals(1, getPromocao("89"));

        Assert.assertEquals(2, getPromocao("90"));
        Assert.assertEquals(2, getPromocao("91"));
        Assert.assertEquals(2, getPromocao("92"));
        Assert.assertEquals(2, getPromocao("93"));
        Assert.assertEquals(2, getPromocao("94"));
        Assert.assertEquals(2, getPromocao("95"));
        Assert.assertEquals(2, getPromocao("96"));
        Assert.assertEquals(2, getPromocao("97"));
        Assert.assertEquals(2, getPromocao("98"));
        Assert.assertEquals(2, getPromocao("99"));
    }

}
